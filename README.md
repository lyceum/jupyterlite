# JupyterLite lyceum

En cours, car la gestion des fichiers n'est pas concluante à ce jour, voir
https://github.com/jtpio/jupyterlite/issues/119

## Outils utilisés

Les sources sont écrites au format `myst` dans le dossier `src`.

`jupytext` permet de les convertir au format notebook dans le dossier `notebooks`.


`jupyterlite` crée une instance du dossier `notebooks` dans le dossier `public`
qui est publié sur framagit et disponible à l'adresse
[https://jupyterlite.lyceum.fr](https://jupyterlite.lyceum.fr).

## Développement

L'utilisation de l'option sync de jupytext permet de réaliser des éditions des
notebooks et de les répércuter sur les sources myst.

    npm run edit

## Analyse des sujjets

Les sujets sont anlysés grâce à la librairie [`radon`](https://radon.readthedocs.io/en/latest/index.html).

    npm run analyze

## Epreuves pratiques

Les épreuves pratiques ont été modifiées à partir du travail de
[Gilles Lassus](https://github.com/glassus/terminale_nsi) sous licence CC-BY-SA.



