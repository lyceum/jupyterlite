---
jupytext:
  text_representation:
    extension: .myst
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernel_info:
  name: python
kernelspec:
  display_name: Python (Pyodide)
  language: python
  name: python
---

# Epreuve pratique: Sujet n°29

Lien vers l'énoncé en `pdf`: [23-NSI-29](./23-NSI-29.pdf).

## Exercice 1 (4 points)

Un arbre binaire est implémenté par la classe `Arbre` donnée ci-dessous.
Les attributs `fg` et `fd` prennent pour valeurs des instances de la classe `Arbre` ou `None`.

```python
class Arbre:
    def __init__(self, etiquette):
        self.v = etiquette
        self.fg = None
        self.fd = None
```

![image](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAABrCAIAAAB/gh4NAAAAA3NCSVQICAjb4U/gAAAH50lEQVR4nO2dbUhTXxzHr/+pKw1tG3MzfMoWpUUJIqErghhB2YssL5luUFCjqITyxURYgyK4vegBomJvilmkTnoSEsrKF65HrS23Utnw2dnA4eZ82HJr/xeDiP5/dXnPds4O5/NqT/zO9/rxuHPPOfcaFwwGKQK+/AM7ACGyEMGYQwRjDhGMOSgK5vF4cSDg8XiwDwU+cQiOouPiwKQCVSemQbEHEwBCBGMOEYw5RDDmEMGYE8OCTSbT6Ogo7BSoE6uCrVarTCYzGAywg6BOTAp+8eLFjh07nE4n7CAxQOwJ1ul05eXlGo1GJBLBzhIDxJ7goqKiwcHBkydPwg4SG8TDDvDX5Ofnw44QS8ReDyb8FUQw5hDBmEMEYw4RjDkoLomTBX+AkB6MOUQw5hDBmEMEYw4RjDkICQ4EAg8fPty5c2dSUhKQfdGJiYllZWWtra2BQAD2wcEjiABut/v69es5OTmFhYU6nW5+fh5I2bm5Ob1eL5PJ1qxZo1KpBgYGgJSNLSALtlqt1dXVPB6Ppum3b99GqJVv376pVKq0tDSZTKbX63/8+BGhhhAEmuCOjg6aptPT01Uq1cjISBRa9Hq9oQ4datRms0WhUehEW/Dc3JxOp9u8eXNBQYFWq52dnY1ygGAw2NfXp1KpRCKRVCqFlSFqRE+w3W7XaDRCoXDfvn1tbW1Ra3chfD5fqEPz+XylUtnd3Q07UUSIhuCuri6FQsHn86urqxEc6YyMjDAMk5WVVVhYqNVqp6enYScCSQQFh7pIcXGxRCJhGGZycjJybbHH7/e3tbXRNC0QCJRKpdFohJ0IDBER7HA4GIbJyMiQSqV6vd7v90eilQgxNjbGMEzonE2r1Xo8HtiJWAFYsNFoVCqVPB5PoVCYzWawxaNJIBAIdejQN3RHRwfsRMsEjOBAINDS0hI6A9FoNBMTE0DKosD4+DjDMOvWrcvPz2cYxul0wk70d4S1JP748eNbt24tPh3W3d0tFovT0tLi4uIW+hiHw7l3755QKFzOlFvkefny5eXLlxd61+l02u12l8tVUlLC4XAWL3XhwoXi4mLQAZdDWPuijUZjRkZGVVUVy8aOHz/ucDiQFWw2m1NTU0+cOLHIZ7xe74oVKxavc+XKlZ6enlgSTFFUbm6uTCZj2diqVatYVog0WVlZ7A+zoaEBSBggILSaRIgERDDmEMGYQwRjDkjBLpervb0dYEHUmJ2dff36dXt7+/z8POws4QLy8tFjx44ZDIbv378DrIkOFotl9+7dfD5/ZmYmPj7+1atXWVlZsEMtDbAefPfu3ZaWFlDVEOTo0aNlZWUWi8Vqtebl5Z05cwZ2orAAI7i/v7+2tlalUgGphiA2m62rq6umpoaiqPj4eJVK1draOjU1BTvX0gAQHAgE5HK5Wq3esGED+2poYjabV65cmZubG3qal5fn9/t7enrgpgoHAIIvXbqUkpJy6tQp9qWQxeVypaam/nqakpJCUZTb7YaXKFzYDrI+fvx48+ZNo9G4yBoDBoRWZn5/SlHUkksOKMBWcE1NjVAorKuroyiqv7/f7XYfOXLk7NmzW7duBREPFYRCocvlCgaDod/jyclJiqIEAgHsXEvDVjBN03a7PfTY6XRyOByxWMzlclkHQ4uCggKfz9fX17dx40aKor58+cLlcmPifj9sBVdXV/96fP/+/c7OToZhWNZEkMzMzF27dtXW1jY1Nfl8vvPnz1dWViYmJsLOtTRkqjJc7ty5Mzw8LBAI0tLSkpOTr127BjtRWICcyZLL5XK5HGBBpMjOzv78+fPQ0BCXyxWLxbDjhEvs3ekOLtnZ2bAj/B3kTzTmEMGYQwRjTrjfwV6vN3R2zwb0r7QHcpg+nw9IGCCEJVgkEqnV6tu3b7NsjMPh/D6jixoikejixYuNjY0s68zPz79582ZiYuLw4cOZmZlAsi0bci+4iPDp06f6+vqmpiaJRELTdGVlJazd4ERwBAkEAu3t7fX19c+ePSspKaFp+uDBg8nJydHMQARHg6mpqSdPnjQ3N797927v3r00Te/Zsyc+PhqTEERwVLHb7c3Nzc3NzQMDA+Xl5TRNb9++PaItEsFw6O3tbWxsfPDgAYfDOXToUFVV1fr16yPREBEMmV/DsYyMDIVCUVFRAfbfBRHBSPBrONbS0lJUVKRQKA4cOADkWj0iGAImk2mhSQW/3z84OGiz2RwOh0QikUqli9Thcrl1dXWLL22R1SQIGAwGq9VaUVHxv+9u27aNoiiPxzM2NhbaQLIQV69e3b9/PxGMIps2bVIqlSyLNDU1LfkZstiAOUQw5hDBmEMEYw4ZZKHFzMzM+Pj4Hy+mp6cve4mCCEaLtra2srKyP1589OjRf18MEyIYLUpLS3/fUqJUKkdHR0tLS5ddkAhGi4SEhNWrV4ceNzQ0tLa2fv36lc0lFGSQhSgej+fcuXNqtZrlTmwiGFG0Wu3Pnz/Z3yiCCEaRYDB448aN06dPJyUlsSxFBKPI+/fvh4eHgVzoRQSjyPPnz7ds2bJ27Vr2pYhgFOns7AwtGrKHCEaRoaGhnJwcIKXIeTCKWCwWUKVID8YcIhhziGDMIYIxhwjGHDKKhgCHw3n69Glvby/LOiaTacnbKZKN7xCYnp7+8OED+598QkKCVCpd/CpFIhhzyHcw5hDBmEMEYw4RjDlEMOYQwZhDBGMOEYw5/wIjMC7grPSIVwAAAABJRU5ErkJggg==)

L’arbre ci-dessus sera donc implémenté de la manière suivante :
```python
a = Arbre(1)
a.fg = Arbre(4)
a.fd = Arbre(0)
a.fd.fd = Arbre(7)
```



Écrire une fonction récursive `taille` prenant en paramètre une instance `a` de la classe
`Arbre` et qui renvoie la taille de l’arbre que cette instance implémente.

Écrire de même une fonction récursive `hauteur` prenant en paramètre une instance `a`
de la classe `Arbre` et qui renvoie la hauteur de l’arbre que cette instance implémente.

Si un arbre a un seul nœud, sa taille et sa hauteur sont égales à 1.
S’il est vide, sa taille et sa hauteur sont égales à 0.

Tester les deux fonctions sur l’arbre représenté ci-dessous :

![image](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM0AAACfCAIAAADVmMrCAAAAA3NCSVQICAjb4U/gAAAP40lEQVR4nO3de0yT5x4H8LICVcm4lFKrqFzU2eJAtAhkyKyuOmdq1EAVxaJMZBcZm5dYbdyauGx2ybKxZCZjkDmCQ1eZZQ1bnAUF0aVCwTZeEAtSUatgxcKAltLS80fP4XiyY9fa91p+n79s8/o8P8iXp+/leZ4GOJ1OCgAoewnvAsCkADkDWICcASxAzgAWIGcAC5AzgAXIGcAC5AxgAXIGsAA5A1iAnAEsQM4AFiBnAAuQM4AFyBnAQiDeBRCLxWL57bffEJmTl5mZyWKxfG/HPwTAPMdnXbx4MTs7m8fj+diOVqstKioqLi5Goih/AOPZ/3A6nQkJCXK53Md2PvroI/gDfhacnwEsQM4AFiBnAAuQM4AFuA7wzsjIiFqtDggIWLZsWVBQEN7lkAbkzAvXr19fvXo1nU4fHh4ODAysr6+fM2cO3kWRA3xueiE/P3/jxo3Xr1/X6/UcDueDDz7AuyLSgPHMU52dnRqN5ueff6ZQKIGBgWKxmMfjDQ4OhoaG4l0aCcB45qlr165NnTo1Pj7e9ZLD4djt9vb2dnyrIgvImafMZnNYWNjES9cwNjAwgF9FZAI585TT6Xz2UZLr31QqFb+KyARy5qmoqCiz2TwRtadPn1IolMjISFyLIg3ImaeSk5NHR0c7OjpcL3U6HY1GS0hIwLcqsoCceWr27NkrV648ePDg6Ojo4ODgJ598snXr1uDgYLzrIgfImRd++OGHnp6eyMhIJpMZEhLy9ddf410RacD9My/ExMS0tbXdvXuXRqPBXFmvQM68FhMTg3cJ5AOfmwALkDOABcgZwALkDGABrgP+q7OzU6lU3rx5c9OmTT42dfXqVQ6Hk5WVNWvWLERqIztYv0mhUCj19fXffPPNlStXdu7cuWTJEt9/JxaL5eLFiwqF4rXXXtu1a9fatWsDAyf3n7RzErNarRUVFYmJiQsWLCgpKRkeHka2fYvFIpfL+Xz+jBkzxGJxV1cXsu2TyCTN2cOHD6VSKZPJ5PP5SqVyfHwc1e5u3bolFounT5/O5/Plcvno6Ciq3RHQpMuZRqMRiUR0Or2wsLC9vR3Lrq1Wq2t4c/V+7do1LHvH12TJmd1uVyqVGRkZ8fHxMpmsv78fx2L0er1UKp09ezaXyy0tLUX885qA/D9nZrO5pKRkzpw5GRkZcrncbrfjXdG/2e12lUolFAojIyMLCwu1Wi3eFaHIn3PW0dFRXFwcEREhEol0Oh3e5TzX/fv3ZTJZTEyMa3gbGhrCuyLk+WHOHA6HSqUSCAQzZsyQSqWPHz/GuyKPuMoWCoWus7fW1la8K0KSX+VscHCwtLSUw+FwudyKigqbzYZ3RS/CaDTKZLL4+Hgul1tSUoLvqSRS/CRnXV1dYrGYwWAIBAKVSoV3OQiYGN5CQ0OFQiHZfygSPA84d+6c++VrKpVKLpevXLnyzTffpNPpzzuMzWYnJiaiUCAyGhoaHj9+/Pf3zWZzY2NjfX09jUbLycnhcrnu22EymcuXL0enxhdHgpwFBQVt2LAhICDgeQfYbDYqlep+iZvRaIyKilIoFCgUiIzp06enp6fTaLTnHdDX10elUt2vsLJarRqNxmg0olCgb/AeUP8ZlUr1/WZETU3N+vXrEakHJVFRUX19fT428vDhQxaLhUg9yIJ5QQALkDOABcgZwALkDGDBTybfabVaBoPh35NXzWazVqulUCiLFy9+duciUvCH8Uyv1/P5/EuXLuFdCIqqq6tjYmL27t374YcfxsXF1dbW4l2Rd0ifs3PnzmVmZj558gTvQlA0ODiYn59/9OjRtrY2nU534MABkUhktVrxrssL5M5ZRUVFdna2VCqdPn063rWg6N69e2lpaTt37nS93Lx5s9ls1uv1+FblFXLnbOnSpQaD4b333sO7EHQtXLiwrq5u4lHBhQsXpkyZQq6tvsl9HTAJtx9rb28/cOCARCIh16UAucezyaalpYXH423cuPHw4cN41+IdyBlpKBQKHo9XUFBQVlbmZlYBMZH7c3PyqKqq2rFjx7Fjx3bt2oV3LS8CckYC3d3dBQUF27dvT0xMVKvVrjcXLlz48ssv41uY5yBnJHDy5EmLxVJeXl5eXj7xZlNT07Jly3Csyit+krNHjx7hXQKKJBKJRCLBuwqfwHUAwALkDGABcgawADkDWICcASwQ/Xqzra0tICAgJyfHxzvgrnV1SFWFuIGBAbvdXlBQ4GZdnSesVqvVam1sbMzMzHzpJQINIgRdv2kymX766afjx48PDAzs2LEDkeflxFwnPDw8/O2333711Vdr1qwRCAS+N/jnn3+eP3/+6dOnW7Zsyc3NTUpK8r1NBOC9sO9/uBb7i0SiiIgIoVCoVCqJs40U4mw2W2lpaXR0tFAo7OjoQLbxGzduSKXSuXPnJiQkSKXSzs5OZNv3FlFy1tHRIZVKXXszlZSUmEwmvCtCkcPhkMvlc+fO5fP5aO8LpNFoiouLmUym6xfb29uLanfPg3PORkZGXFtpToa95pxO5/j4uFKpTEpKysjIaGhowKxf155+IpEoLCyMz+dXVFRgvMsabjnTaDSFhYWuH1sul5N0DymvqFQqLpebmpqqVCrxqsH1hy0QCFzbECmVyrGxMQz6xTpnDx48kMlk8+fPZ7PZMpns0aNHGBeAi8uXLy9fvjwhIUEul6O9t7eHTCZTaWlpRkYGg8EoLCxsampCtTCMcma1WpVKpWuzQpFIRPbdvDzX3NwsEAhiY2NLS0uJeU3T3d0tk8leeeWV2NhYsVh8+/ZtNHpB/b7GjRs3Kisrf/zxx3nz5uXl5eXm5oaEhKDaIwZcu3c7HA43x9y7d+/UqVPd3d1ZWVk8Hu9522YlJSUtWLAAnTL/P6vVarFY/v6+RqOprq5WKBSxsbHZ2dlZWVluNpOjUCihoaHu9wJ71ovn7JdffiksLHT/3x0Ox9DQUHBwMI1Ge95tw6CgoAsXLpBrRYlOp+PxeKtWrXJzjMFgGBsbmzt3rpv7pXfv3mWz2RUVFSjU+FwLFizo7e11U9XY2JjNZqPRaG6+K8hut6emptbV1XnY6Ys/D7h//75QKDx69Kj7w8bHx93fmF63bt3/3ceQyJxOZ2xsrFwu97GdyspKlUqFSEmeGxoaunnz5syZM31pRK1W79mzx/PjfXruNGXKlIiICF9aoFAok/0LtiYHAj0CA34McgawADkDWIBzIxSZzearV6+uWLEC70K8MDo6eu/evYmXVCo1Li7O92ZRzFl/f39ra2tISEh6ejqh5kJhpqCg4NKlS+Rai3X27NkNGzZMvIyMjDSZTL43i1bOampq8vLy2Gy2yWSaNm3a2bNn/Xuzxb87fvy4Uql0f6uTgLRaLY/Hm/iiBaQGCFSGGYvFsn379qNHjzY3N+v1ehaLdfDgQTQ6Iqw7d+4cPHhQLBbjXYjXdDpdampq+H+EhoYi0iwqORseHhaLxW+//TaFQqFSqUuXLr19+zYaHRGTw+HYtm3bxx9/jPEDJURotdqBgYGNGzfyeLwjR46Mjo4i0iwqOWMwGBKJZOrUqRQKpbm5ubKyctOmTWh0REyfffZZaGjo7t278S7Ea4ODgwaDobW1NTs7WygUlpWVrV+/HpGW0b3eXLx4sVarTUxMzM3NRbUj4mhubj527NjVq1dJt3UUhUKZMmVKU1NTcnKya65Denp6SkqKWq1OT0/3sWV0LwMbGhoePHgQFxeXmZmJ1AhMcPv27YuKipJIJDt27Pj+++9d62h0Oh3edXkkODg4IyNjYkINl8udOnUqIhvhopuzsLCwmTNnfvfdd11dXf697/oEoVAoEAhYLBaLxQoLC6NSqSwWy8fVcphRq9U8Hm9i1lBfX5/FYomJifG9ZVQ+N9VqdVFR0eXLl12/X9fcIT+YduaJ4uLiiX+fOHGipaVFJpPhWI9X5s2bp9Vqjxw58vnnn1ut1t27dy9atAiR3a9QGc/YbLbBYPj000+dTqfVat27dy+Hw1myZAkafQEEMRgMhUJx6tQpBoMRGRnZ09Nz5swZRG6hoZKz8PDwmpqaqqoqBoNBp9N7enp+/fXX4OBgNPoism3btpHrYQCFQlmxYsWdO3d0Ol1nZ+eVK1fi4+MRaRat681ly5Z1dXX19PSEhIQwGAyUegFoCAgIQPzhDYr3NQICAhA5hQR+YDI+3gbYg5wBLEDOABZ8Oj/r7e1tbW31sYK//vrLxxZwYTQafZ+Ecv36dfeLQFFSW1vr4wIibx8SvHjOOBxOZWXlO++888ItuNBoNNJNTZs/f/7+/fvHx8d9bOfVV18tKyurra1FZOczD+Xn53u+7tKNnJwczw8m6D57k4dWq127dm1VVRWPx8O7FhTB+RnOkpOTq6qqNm/erNFo8K4FRZAz/PF4vPLy8nXr1t28eRPvWtACOSOEdevWffnll2vWrDEYDHjXggpYV0cUubm5AwMDq1atampqYrFYeJeDMMgZgbz//vsmk2n16tWNjY2+b1xCKHC9STj79++/fPlyXV2dP83Yg5wRjtPp3LVr1927d2tra8kyEfcfQc6IyOFwbNmyxW63nz592vM9E4kMrjeJiEqlnjhxYmRkZOfOnf4xEEDOCCo4OPjMmTOdnZ1ebZtIWJAz4po2bVptbW1jY+MXX3yBdy2+gvsahBYeHv7HH3+8/vrr4eHhvk9ZwBHkjOiYTObvv/++fPnysLAwr6ZIEArkjCgMBoObiUZlZWV5eXkjIyP/OK0jOjqagHdD4L4GIeh0urS0tOjoaDfHWK1Wp9Pp2h3nefr7+w8fPrxv3z6kC/QVjGeEYLPZkpKSmpubfWxHIpHYbDZESkIWXG8CLEDOABYgZwALkDOABcgZwAJcb5KD3W6/cuXK0NAQl8sl4744kDMS6OnpWbNmzejoKJ1Ov3Xr1smTJ7Fc74kI+Nwkgfz8/ISEBL1e39LScujQoYKCAt+XKGMMxjOi6+7uPn/+fHd3t2tfxaKiokWLFtntdnLtWwg5IzqNRhMdHR0YGHjo0KG+vr433nhj69ateBflNfjcJLre3l6n05mZmTk8PBweHv7uu+/u3bsX76K8BuMZ0dntdqPRqFAoXF8jx+fzBQLBnj17Zs+ejXdpXoDxjOhc33j31ltvuV7y+Xyn03njxg1ci/Ia5IzokpOTKRSK0Wh0vXzy5InT6STd1y1CzoguKSkpLS1tz549IyMjDofj0KFDbDY7JSUF77q8Azkjgerq6idPnjCZTDqd3tLSUl1dTbrvZ4brABKYNWtWU1OT0WgcGxsj6Vb5kDPSmDlzJt4lvDiSDb+ApCBnAAuQM4AFOD8jiv7+/tOnT/vYSHt7e2pqKiL1IAtyRghz5sxJSUnxPWdBQUFpaWmIlIQsWCcMsADnZwALkDOABcgZwALkDGABcgawADkDWICcASxAzgAWIGcAC/8CzdmABq9jS1EAAAAASUVORK5CYII=)

```{code-cell}
# Coder ici la réponse à l'exercice 1
```

## Exercice 2 (4 points)

La méthode `insert` de la classe `list` permet d’insérer un élément dans une liste à un
`indice` donné.

Le but de cet exercice est, *sans utiliser cette méthode*, d’écrire une fonction `ajoute`
réalisant cette insertion en produisant une nouvelle liste.

Cette fonction `ajoute` prend en paramètres trois variables `indice`, `element` et `liste`
et renvoie une liste `L` dans laquelle les éléments sont ceux de la liste `liste` avec, en
plus, l’élément `element` à l’indice `indice`.
On considère que les variables `indice` et `element` sont des entiers positifs et que les
éléments de `liste` sont également des entiers positifs.
Les éléments de la liste `liste`, dont les indices sont supérieurs ou égaux à `indice`
apparaissent décalés vers la droite dans la liste `L`.
Si `indice` est supérieur ou égal au nombre d’éléments de la liste `liste`, l’élément
element est ajouté dans `L` après tous les éléments de la liste `liste`.

Exemple :
```python
>>> ajoute(1, 4, [7, 8, 9])
[7, 4, 8, 9]
>>> ajoute(3, 4, [7, 8, 9])
[7, 8, 9, 4]
>>> ajoute(4, 4, [7, 8, 9])
[7, 8, 9, 4]
```

Compléter et tester le code ci-dessous :

```python
def ajoute(indice, element, liste):
    nbre_elts = len(liste)
    L = [0 for i in range(nbre_elts + 1)]
    if ...:
        for i in range(indice):
            L[i] = ...
        L[...] = ...
        for i in range(indice + 1, nbre_elts + 1):
            L[i] = ...
    else:
        for i in range(nbre_elts):
            L[i] = ...
        L[...] = ...
    return L
```

```{code-cell}
# Code à compléter de l'exercice 2
```
